# ROS1 ISC camera driver Node
==============================

    This is the NODE to pubish some kinds of image from the ITD Lab's ISC(Intelligent stereo camera).
    This driver supports XC model and VM model.
    XC model:The image size is 1024x720. 1280x720 if stereo mode.
    VM model:The image size is 640x480.

# Topics
    '/isc_camera/image_raw' - Calibrated image of left sensor (*sensor_msgs::msg::Image*)
    '/isc_camera/depth'     - depth data (*sensor_msgs::msg::Image*)
    '/isc_camera/parallax'  - Calibrated image and parallax data (*sensor_msgs::msg::Image*)
    '/isc_camera/left'      - Calibrated image of left sensor (*sensor_msgs::msg::Image*)
    '/isc_camera/right'     - Calibrated image of right sensor (*sensor_msgs::msg::Image*)
    '/isc_camera/demo'      -  Raw image and colored parallax data (*sensor_msgs::msg::Image*)
    '/isc_camera/point_cloud' - Point cloud as left hand coordinate (*sensor_msgs::msg::PointCloud2*)

    Note - image_raw and parallax includes Camera parameters(struct CameraParam) in the top-left of the image.
            You can Convert it with parallax data and CameraParam to XYZ.
    Note - /isc_camera/depth is depth distance as 16bit per pixel and the unit is millimeter.
           65535 if over 65535 millimeter.
           0 if no distance data
    Note - The format of /isc_camera/parallax is below.
    +------------------------+
    | raw image              |
    | 8bit per pixel         |
    | 1024 x 720 size        |
    +------------------------+
    | parallax integer part  |           15           8 7           0 bit 
    | 8bit per each          |           +-------------+-------------+
    | 1024 x 720             | --------->|integer part |decimal part |
    +------------------------+           +-------------+-------------+
    | parallax decimal part  |                                ^
    | 8bit per each          |                                |
    | 1024 x 720             | -------------------------------+
    +------------------------+
    VM model is 640x480 instead of 1024x720.

# Installation
## Installation for XC model
	sudo apt-get install usb-1.0
	sudo apt-get install libconfuse-dev
	sudo dpkg -i lib/iscsdk2_0.9.3K-1_amd64.deb

    Please download FTDI D3xx driver from the below and install.
    https://www.ftdichip.com/Drivers/D3XX.htm

## Installation for VM model
	sudo apt-get install usb-1.0
	sudo apt-get install libconfuse-dev
	git clone git://developer.intra2net.com/libftdi
	mkdir -p libftdi/build
	cd libftdi/build
	sudo cmake –DCMAKE_INSTALL_PREFIX=”/usr/local/src/ftdi” ../
	sudo make
	sudo make install

	sudo dpkg -i lib/iscsdk1_0.9.3K-1_amd64.deb

# Build
    catkin_make
    Use CMakeList.txt.XC for XC model, CMakeList.txt.VM for VM model

# Run
## Run as Superuser
	sudo su
	source /opt/ros/kinetic/setup.bash
	source devel/setup.bash

## Run XC model
    rosrun isc_driver xc_driver_node
    or
    roslaunch isc_driver xc_driver.launch

## Run VM model
    rosrun isc_driver vm_driver_node
    or
    roslaunch isc_driver vm_driver.launch

# Parameters
    frame_id: (string default:isc_camera) - Frame id
    fps:  (integer default:60) - Frame per sec(30 or 60). Actual FPS is depend on your system and publishing topics.
    stereo: (bool default:0)   - publish stereo image to /isc_camera/left and /isc_camera/right
                               - If this switch is on, publish only left and right, do not publish raw, depth, parallax, cloud2 and demo.
    raw: (bool default:1)      - publish raw image to /isc_camera/image_raw
    depth: (bool default:1)    - publish depth distance to /isc_camera/depth
    parallax: (bool default:0) - publish raw image and parallax to /isc_camera/parallax
    demo: (bool default:0)     - publish raw image and colored parallax to /isc_camera/demo
    cloud2: (bool default:1)   - publish point cloud2 to /isc_camera/point_cloud
    auto: (bool default:1)     - auto shutter control
    gain: (integer default:16) - Gain value if shuttter control is not auto
                               - The value is from 16 to  64 for VM model
                               - The value is from  0 to 720 for XC model
    exposure: (integer default:479) - exposure value if shuttter control is not auto
                               - The value is from  1 to 479 for VM model
                               - The value is from  2 to 748 for XC model(2 is brightest,  748 is darkest)
    noise_filter:(int default:8) - Noise filter level(0 to 8, The higher number is less noise)

