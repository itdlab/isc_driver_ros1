//   Copyright 2019 ITDLab corporation
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/**
 * @file    isc_driver.cpp
 * @brief    Driver of ISC camera
 * @date    2019/10/08
 * @author  Katsuichi Kigasawa
 */

#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <isc_driver/isc_driverConfig.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <pcl_conversions/pcl_conversions.h>
#include <opencv2/opencv.hpp>

#include <string>
#include <mutex>

#include "ISCSDKLib.h"
#include "isc_driver/isc_wrapper.hpp"

#define STOP_WATCH 0

#define INVALID_DISPARITY 0
#define COLOR_PALETTE_SIZE 256
struct _stRGB {
  uint8_t R;
  uint8_t G;
  uint8_t B;
};
void make_color_table(struct _stRGB *color_table);
void callback_param(isc_driver::isc_driverConfig &config, uint32_t level);
static isc_camera::ISCWrapper *isc_ = NULL;

int main(int argc, char **argv)
{
  int count = 0;
  ros::init(argc, argv, "isc_driver");
  ros::NodeHandle nh("~");

  // Setup parameter
#if (MODEL==1)
  std::string model("VM");
#elif (MODEL==2)
  std::string model("XC");
#endif
  std::string frame_id("isc_camera");
  int fps = 60;
  bool publish_depth = true;
  bool publish_stereo = false;
  bool publish_raw = true;
  bool publish_parallax = false;
  bool publish_cloud2 = true;
  bool publish_demo = false;
  bool shutter_auto = true;
  int shutter_gain = 16;
  int shutter_exposure = 479;
  int noise_filter = 8;
  int width = 640;
  int height = 480;
  {
    int v;
    std::string str;
    nh.getParam("frame_id", frame_id);
    v = fps;
    nh.getParam("fps", v);
    fps = v;

    v = noise_filter;
    nh.getParam("noise_filter", v);
    noise_filter = v;

    v = publish_stereo;
    nh.getParam("stereo", v);
    publish_stereo =  v ? true: false;

    v = publish_depth;
    nh.getParam("depth", v);
    publish_depth =  v ? true: false;
    v = publish_raw;
    nh.getParam("raw", v);
    publish_raw =  v ? true: false;
    v = publish_parallax;
    nh.getParam("parallax", v);
    publish_parallax =  v ? true: false;
    v = publish_cloud2;
    nh.getParam("cloud2", v);
    publish_cloud2 =  v ? true: false;
    v = publish_demo;
    nh.getParam("demo", v);
    publish_demo =  v ? true: false;
    if ( publish_stereo == true ) {
      publish_raw = false;
      publish_parallax = false;
      publish_cloud2 = false;
      publish_demo = false;
    }

    v = shutter_auto;
    nh.getParam("auto", v);
    shutter_auto =  v ? true: false;
    nh.getParam("gain", shutter_gain);
    nh.getParam("exposure", shutter_exposure);
    if( model == "XC" )
    {
      width = 1024;
      height = 720;
      if( publish_stereo ) {
        width = 1280;
      }
    }

    ROS_INFO("model: %s" , model.c_str());
    ROS_INFO("frame_id: %s" , frame_id.c_str());
    ROS_INFO("fps: %d" , fps);
    ROS_INFO("publish_stereo: %d" , publish_stereo);
    ROS_INFO("publish_raw: %d" , publish_raw);
    ROS_INFO("publish_depth: %d" , publish_depth);
    ROS_INFO("publish_parallax: %d" , publish_parallax);
    ROS_INFO("publish_cloud2: %d" , publish_cloud2);
    ROS_INFO("publish_demo: %d" , publish_demo);
    ROS_INFO("shutter_auto: %d" , shutter_auto);
    ROS_INFO("shutter_gain: %d" , shutter_gain);
    ROS_INFO("shutter_exposure: %d" , shutter_exposure);
    ROS_INFO("noise_filter: %d" , noise_filter);
    ROS_INFO("width: %d" , width);
    ROS_INFO("height: %d" , height);
  }

  struct _stRGB color_table[COLOR_PALETTE_SIZE];
  make_color_table(color_table);

  ros::Rate loop_rate(500);

  /*
   * Opening
   */
  isc_ = new isc_camera::ISCWrapper;
  int ret = isc_->open(publish_stereo);
  if( ret != 0 )
  {
    ROS_ERROR("Camera open error %d", ret);
  }
  else
  {
    // Dymanic reconfigure
    boost::recursive_mutex mutex_;
    dynamic_reconfigure::Server<isc_driver::isc_driverConfig> srv(mutex_);
    dynamic_reconfigure::Server<isc_driver::isc_driverConfig>::CallbackType f;
    f = boost::bind(&callback_param, _1, _2);
    srv.setCallback(f);
    isc_driver::isc_driverConfig config;
    config.shutter_auto = shutter_auto;
    config.shutter_gain = shutter_gain;
    config.shutter_exposure = shutter_exposure;
    config.noise_filter = noise_filter;
    srv.updateConfig(config);

    // Initialize for camera
    isc_->set_shutter_control(shutter_auto, shutter_gain, shutter_exposure);
    isc_->set_noise_filter(noise_filter);
    float base_length;
    float d_inf;
    float bf;
    struct ISCSDKLib::CameraParamInfo info;
    isc_->get_camera_info(&info);
    base_length = info.fBaseLength;
    d_inf = info.fD_INF;
    bf = info.fBF;
    ROS_INFO("CameraParam %f %f %f",  base_length, d_inf, bf );

    int xCenter = width/2;
    int yCenter = height/2;
    int image_size = width*height;
    uint8_t *img_buffer = (uint8_t*)malloc(image_size*3);
    uint8_t *right_buffer = (uint8_t*)malloc(image_size*3);
    float *parallax_buffer = (float*)malloc(image_size*sizeof(float));
    uint8_t *demo_buffer = (uint8_t*)malloc(image_size*3*2);
    uint16_t *depth_buffer = (uint16_t*)malloc(image_size*sizeof(uint16_t));

    // Initialize messages
    std_msgs::Header header_depth;
    header_depth.frame_id = frame_id;
    sensor_msgs::CameraInfo camera_depth_info;
    camera_depth_info.width = width;
    camera_depth_info.height = height;
    camera_depth_info.header.frame_id = frame_id;

    std_msgs::Header header;
    header.frame_id = frame_id;
    sensor_msgs::CameraInfo camera_info;
    camera_info.width = width;
    camera_info.height = height;
    camera_info.header.frame_id = frame_id;
    
    std_msgs::Header header_image_parallax;
    header_image_parallax.frame_id = frame_id;
    sensor_msgs::CameraInfo camera_raw_parallax_info;
    camera_raw_parallax_info.width = width;
    camera_raw_parallax_info.height = height*3;
    camera_raw_parallax_info.header.frame_id = frame_id;

    std_msgs::Header header_demo;
    header_demo.frame_id = frame_id;
    sensor_msgs::CameraInfo camera_demo_info;
    camera_demo_info.width = width*2;
    camera_demo_info.height = height;
    camera_demo_info.header.frame_id = frame_id;

    sensor_msgs::PointCloud2 ros_pcl_;

    cv::Mat mat_depth(height, width, CV_16UC1, depth_buffer);
    cv::Mat mat_image(height, width, CV_8UC1, img_buffer);
    cv::Mat mat_right(height, width, CV_8UC1, right_buffer);
    cv::Mat mat_raw_parallax_image(height*3, width, CV_8UC1, img_buffer);
    cv::Mat mat_demo_image(height, width*2, CV_8UC3, demo_buffer);

    int w;
    int h;
    isc_->get_size(&w, &h);
    if( w == width && h == height )
    {
      isc_->start_grab(publish_stereo);
      int skip_count_init = 60 / fps;
      int skip_count = skip_count_init;
      ros::Time prev_time = ros::Time::now();

      if ( publish_stereo == false ) {
        /*
         * Publish images with parallax
         */
        image_transport::ImageTransport it_depth(nh);
        image_transport::CameraPublisher pub_depth = it_depth.advertiseCamera("/isc_camera/depth", 10);
        image_transport::ImageTransport it(nh);
        image_transport::CameraPublisher pub = it.advertiseCamera("/isc_camera/image_raw", 10);
        image_transport::ImageTransport it_raw_parallax(nh);
        image_transport::CameraPublisher pub_raw_parallax = it_raw_parallax.advertiseCamera("/isc_camera/parallax", 10);
        image_transport::ImageTransport it_demo(nh);
        image_transport::CameraPublisher pub_demo = it_raw_parallax.advertiseCamera("/isc_camera/demo", 10);
        ros::Publisher pub_pcl = nh.advertise<sensor_msgs::PointCloud2>("/isc_camera/point_cloud", 10);
        while(ros::ok())
        {
          if( isc_->get_images(img_buffer, parallax_buffer) == true ) {
            ros::Time now_time = ros::Time::now();
            skip_count--;
            if( skip_count == 0 ) {
              skip_count = skip_count_init;
              header.stamp = ros::Time::now();
              header_image_parallax.stamp = ros::Time::now();
#if (STOP_WATCH==1)
              ros::Duration duration = header.stamp - prev_time;
              ROS_INFO("Duration %u.%u",  duration.sec, duration.nsec/1000000);
              prev_time = header.stamp;
              struct  isc_camera::ISCWrapper::CameraParam *info = (struct isc_camera::ISCWrapper::CameraParam *)img_buffer;
              ROS_INFO("CameraParam %x %f %f %f",  info->magic, info->d_inf, info->bf, info->base_length);
              ROS_INFO("CameraParam %i %i %i",  info->d_inf_int, info->bf_int, info->base_length_int);
#endif
    
              /*
               * Publish image
               */
              if( publish_raw ) {
                sensor_msgs::ImagePtr msg = cv_bridge::CvImage(header, "mono8", mat_image).toImageMsg();
                pub.publish(*msg, camera_info, now_time);
              }
              /*
               * Publish depth
               */
              if( publish_depth ) {
                int idx = 0;
                for( int y=0; y<height; y++ )
                {
                  for( int x = 0; x< width; x++ )
                  {
                    float d = parallax_buffer[idx] - d_inf;
                    if( d >= 1.0F )
                    {
                      uint32_t t = (uint32_t)(bf / d *1000.0F);
                      if( t > 65535 )
                      {
                        depth_buffer[idx] = 65535;
                      }
                      else {
                        depth_buffer[idx] = t;
                      }
                    }
                    else
                    {
                      depth_buffer[idx] = 0;
                    }
                    idx++;
                  }
                }
                sensor_msgs::ImagePtr msg = cv_bridge::CvImage(header_depth, "mono16", mat_depth).toImageMsg();
                pub_depth.publish(*msg, camera_depth_info, now_time);
              }
              /*
               * Publish image and parallax
               */
              if( publish_parallax ) {
                uint8_t *dp1 = &img_buffer[image_size];
                uint8_t *dp2 = &img_buffer[image_size*2];
                for( int idx=0;idx<image_size;idx++)
                {
                  int d = (int)(parallax_buffer[idx] * 256);
                  *dp1++ = (int8_t)(d >> 8);
                  *dp2++ = (int8_t)d;
                }
                sensor_msgs::ImagePtr msg_raw_parallax = cv_bridge::CvImage(header_image_parallax, "mono8", mat_raw_parallax_image).toImageMsg();
                pub_raw_parallax.publish(*msg_raw_parallax, camera_raw_parallax_info, now_time);
              }
              /* 
               * Publish demo images with parallax color
               */
              if (publish_demo) {
                uint8_t *sp = img_buffer;
                float *fp = parallax_buffer;
                int width3 = width*3;
                uint8_t* dp1 = demo_buffer;
                uint8_t* dp2 = demo_buffer +width3;
                for ( int y=0; y<height; y++ ) {
                  for ( int x = 0; x < width ; x++ ) {
                    *dp1++ = *sp;
                    *dp1++ = *sp;
                    *dp1++ = *sp;
                    sp++;
    
                    int i = (int)(*fp++);
                    *dp2++ = color_table[i].R;
                    *dp2++ = color_table[i].G;
                    *dp2++ = color_table[i].B;
                  }
                  dp1 += width3;
                  dp2 += width3;
                }
                sensor_msgs::ImagePtr msg_demo = cv_bridge::CvImage(header_demo, "rgb8", mat_demo_image).toImageMsg();
                pub_demo.publish(*msg_demo, camera_demo_info, now_time);
              }
    
              /*
               * Publish Point cloud
               */
              if( publish_cloud2 ) {
                pcl::PointCloud<pcl::PointXYZRGB> cloud_pcl_;
                cloud_pcl_.width = 1;
                cloud_pcl_.height = 0;
                int idx = 0;
                pcl::PointXYZRGB xyz;
                for( int y=0; y<height; y++ )
                {
                  for( int x = 0; x< width; x++ )
                  {
                    uint8_t itensity = img_buffer[idx];
                    float d = parallax_buffer[idx++] - d_inf;
                    if( d >= 1.0F )
                    {
                      xyz.r = itensity;
                      xyz.g = itensity;
                      xyz.b = itensity;
                      xyz.z = bf / d;
                      xyz.x = base_length / d * (x - xCenter);
                      xyz.y = base_length / d * (yCenter - y);
                      cloud_pcl_.points.push_back(xyz);
                      cloud_pcl_.height++;
                    }
                  }
                }
//              ROS_INFO("%f %d" , d_inf, cloud_pcl_.height);
                if( cloud_pcl_.height > 0 )
                {
                  pcl::toROSMsg(cloud_pcl_, ros_pcl_);
                  ros_pcl_.header.frame_id = frame_id;
                  ros_pcl_.header.stamp = now_time;
                  pub_pcl.publish(ros_pcl_);
                }
              }
            }
          }
     
          ros::spinOnce();
          loop_rate.sleep();
      
        }
      }
      else {
        /*
         * Publish left and right image
         */
        image_transport::ImageTransport it_left(nh);
        image_transport::CameraPublisher pub_left = it_left.advertiseCamera("/isc_camera/left/image_raw", 10);
        image_transport::ImageTransport it_right(nh);
        image_transport::CameraPublisher pub_right = it_right.advertiseCamera("/isc_camera/right/image_raw", 10);
        while(ros::ok()) {
          if( isc_->get_stereo_images(img_buffer, right_buffer) == true ) {
            ros::Time now_time = ros::Time::now();
            skip_count--;
            if( skip_count == 0 ) {
              skip_count = skip_count_init;
              header.stamp = ros::Time::now();
              header_image_parallax.stamp = ros::Time::now();
#if (STOP_WATCH==1)
              ros::Duration duration = header.stamp - prev_time;
              ROS_INFO("Duration %u.%u",  duration.sec, duration.nsec/1000000);
              prev_time = header.stamp;
              {
                struct  isc_camera::ISCWrapper::CameraParam *info = (struct isc_camera::ISCWrapper::CameraParam *)img_buffer;
                ROS_INFO("CameraParam %x %f %f %f",  info->magic, info->d_inf, info->bf, info->base_length);
                ROS_INFO("CameraParam %i %i %i",  info->d_inf_int, info->bf_int, info->base_length_int);
              }
              {
                struct  isc_camera::ISCWrapper::CameraParam *info = (struct isc_camera::ISCWrapper::CameraParam *)right_buffer;
                ROS_INFO("--- CameraParam %x %f %f %f",  info->magic, info->d_inf, info->bf, info->base_length);
                ROS_INFO("--- CameraParam %i %i %i",  info->d_inf_int, info->bf_int, info->base_length_int);
              }
#endif
    
              {
                sensor_msgs::ImagePtr msg = cv_bridge::CvImage(header, "mono8", mat_image).toImageMsg();
                pub_left.publish(*msg, camera_info, now_time);
              }
              {
                sensor_msgs::ImagePtr msg = cv_bridge::CvImage(header, "mono8", mat_right).toImageMsg();
                pub_right.publish(*msg, camera_info, now_time);
              }
            }
          }
          ros::spinOnce();
          loop_rate.sleep();
        }
      }
      isc_->stop_grab();
    }
    else {
      ROS_ERROR("Not match Camera model and image size");
    }
    isc_->stop_grab();
    srv.clearCallback();
    free(img_buffer);
    free(right_buffer);
    free(parallax_buffer);
    free(demo_buffer);
  }

  /*
   *  Closing
   */
  isc_->close();
  delete isc_;
  isc_ = NULL;
}

void callback_param(isc_driver::isc_driverConfig &config, uint32_t level) {

  bool shutter_auto_new = config.shutter_auto;
  int shutter_gain_new = config.shutter_gain; 
  int shutter_exposure_new = config.shutter_exposure;
  int noise_filter_new = config.noise_filter;
  if( isc_ != NULL ) {
    isc_->set_shutter_control( shutter_auto_new, shutter_gain_new, shutter_exposure_new );
    isc_->set_noise_filter( noise_filter_new );
  }
}


void make_color_table(struct _stRGB *color_table)
{
  short STEP_ = 12;
  int cnt = 0;
  int bnd1 = INVALID_DISPARITY + 1;
  int bnd2 = STEP_;
  int bnd3 = STEP_ * 2;
  int bnd4 = STEP_ * 3;
  int bnd5 = STEP_ * 4;
  int bnd6 = COLOR_PALETTE_SIZE / 2;

  for ( ; cnt<bnd1 ; cnt++) {
    color_table[cnt].B = 0;
    color_table[cnt].G = 0;
    color_table[cnt].R = 0;
  }
  for ( ; cnt<bnd2 ; cnt++) {
    color_table[cnt].B = 255;
    color_table[cnt].G = (unsigned char)(255 * ((float)(cnt - bnd1) / (float)(bnd2 - bnd1)));
    color_table[cnt].R = 0;
  }
  for ( ; cnt<bnd3 ; cnt++) {
    color_table[cnt].B = (unsigned char)(255 * (1.0 - ((float)(cnt - bnd2) / (float)(bnd3 - bnd2))));
    color_table[cnt].G = 255;
    color_table[cnt].R = 0;
  }
  for ( ; cnt<bnd4 ; cnt++) {
    color_table[cnt].B = 0;
    color_table[cnt].G = 255;
    color_table[cnt].R = (unsigned char)(255 * ((float)(cnt - bnd3) / (float)(bnd4 - bnd3)));
  }
  for ( ; cnt<bnd5 ; cnt++) {
    color_table[cnt].B = 0;
    color_table[cnt].G = (unsigned char)(255 * (1.0 - ((float)(cnt - bnd4) / (float)(bnd5 - bnd4))));
    color_table[cnt].R = 255;
  }
  for ( ; cnt<bnd6 ; cnt++) {
    color_table[cnt].B = (unsigned char)(255 * ((float)(cnt - bnd5) / (float)(bnd6 - bnd5)));
    color_table[cnt].G = 0;
    color_table[cnt].R = 255;
  }
  for ( ; cnt<COLOR_PALETTE_SIZE ; cnt++) {
    color_table[cnt].B = 255;
    color_table[cnt].G = 255;
    color_table[cnt].R = 255;
  }
}

