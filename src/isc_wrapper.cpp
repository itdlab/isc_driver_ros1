//   Copyright 2019 ITDLab corporation
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

/**
 * @file  isc_wrapper.cpp
 * @brief  Wrapper of ISC camera SDK
 * @date  2019/10/08
 * @author  Katsuichi Kigasawa
 */
#include "isc_driver/isc_wrapper.hpp"

namespace isc_camera
{

ISCWrapper::ISCWrapper()
{
  is_open_ = false;
  is_start_ = false;
  image_ = (uint8_t *)malloc(MAX_WIDTH * MAX_HEIGHT);
  pallarax_ = (uint8_t *)malloc(MAX_WIDTH * MAX_HEIGHT);
  depth_ = (float *)malloc(MAX_WIDTH * MAX_HEIGHT * sizeof(float));
  fullframe_ = (uint16_t*)malloc(MAX_WIDTH * MAX_HEIGHT * sizeof(uint16_t));
}

ISCWrapper::~ISCWrapper()
{
  this->close();
  free(image_);
  free(pallarax_);
  free(depth_);
}


int ISCWrapper::open(bool stereo)
{
  int ret = -100;
  lock_.lock();
  if (is_open_ == false) {
    mask_area_width_ = 0;
    ret = ISCSDKLib::OpenISC();
    if (ret == 0) {
      ret = ISCSDKLib::GetCameraParamInfo(&info_);
      if (ret == 0) {
        if (info_.nImageWidth == 1280) {
          if( stereo == false ) {
            mask_area_width_ = 256;
          }
          info_.nImageWidth -= mask_area_width_;
          model_ = 1;
        } else {
          model_ = 0;
        }

        camera_param_.magic = 0x12345678;
        camera_param_.bf = info_.fBF;
        camera_param_.bf_int = info_.fBF *1000;
        camera_param_.d_inf = info_.fD_INF;
        camera_param_.d_inf_int = info_.fD_INF *1000;
        camera_param_.base_length = info_.fBaseLength;
        camera_param_.base_length_int = info_.fBaseLength *1000;
      }
    } else {
      ;
    }

    if (ret == 0) {
      is_open_ = true;
      shutter_auto_ = 1;
      shutter_gain_ = -1;
      shutter_exposure_ = -1;
      ISCSDKLib::SetShutterControlMode(shutter_auto_);
      noise_filter_ = 8;
      ISCSDKLib::SetNoiseFilter(noise_filter_);
    }
  }

  lock_.unlock();
  return ret;
}

void ISCWrapper::close()
{
  if ( is_open_ == true ) {
    this->stop_grab();
    lock_.lock();
    is_open_ = false;
    ISCSDKLib::CloseISC();
    lock_.unlock();
  }
}

bool ISCWrapper::start_grab(bool stereo)
{
  bool ret = false;
  lock_.lock();
  if (is_open_ == true) {
    if ( is_start_ == false) {
      int r = 0;
      if ( stereo == false ) {
        r = ISCSDKLib::StartGrab(2);
      }
      else {
        r = ISCSDKLib::StartGrab(3);
      }
      if (r == 0) {
        is_start_ = true;
        ret = true;
      }
    }
    else {
      ret = true;
    }
  }
  lock_.unlock();
  return ret;
}

bool ISCWrapper::stop_grab()
{
  bool ret = false;
  lock_.lock();

  if (is_open_ == true) {
    int r = 0;
    r = ISCSDKLib::StopGrab();
    if (r == 0) {
      is_start_ = false;
      ret = true;
    }
  }
  lock_.unlock();
  return ret;
}

bool ISCWrapper::get_images(uint8_t * image, float * depth)
{
  bool ret = false;
  lock_.lock();
  int r = 0;
  if (is_open_ == true) {
    r = ISCSDKLib::GetImage(pallarax_, image_, 0);
    if (r == 0) {
      r = ISCSDKLib::GetDepthInfo(depth_);
      /*
       * Convert the original position to Upper-Left
       */
      switch (model_) {
        case 0:                         // VM
          {
            int size = info_.nImageWidth * info_.nImageHeight;
            uint8_t * sp = image_ + (size - 1);
            uint8_t * dp = image;
            float * sfp = depth_ + (size - 1);
            float * dfp = depth;
            for (uint y = 0; y < info_.nImageHeight; y++) {
              for (uint x = 0; x < info_.nImageWidth; x++) {
                *dp++ = *sp--;
                *dfp++ = *sfp--;
              }
            }
            ret = true;
          }
          break;
        case 1:                         // XC
          {
            int w = info_.nImageWidth +256;
            int size = w * info_.nImageHeight;
            uint8_t * sp = image_ + (size - w + 256);
            uint8_t * dp = image;
            float * sfp = depth_ + (size - w + 256);
            float * dfp = depth;
            int w_fp = info_.nImageWidth * sizeof(float);
            for (uint y = 0; y < info_.nImageHeight; y++) {
              memcpy(dp, sp, info_.nImageWidth);
              sp -= w;
              dp += info_.nImageWidth;
              memcpy(dfp, sfp, w_fp);
              sfp -= w;
              dfp += info_.nImageWidth;
            }
            ret = true;
          }
          break;
        default:
          break;
      }
      /*
       * Set camera parameters into the image
       */
      {
        memcpy( image, &camera_param_, sizeof(camera_param_));
      }
    }
  }
  lock_.unlock();

  return ret;
}

bool ISCWrapper::get_stereo_images(uint8_t * left_image, uint8_t * right_image)
{
  bool ret = false;
  lock_.lock();
  int r = 0;
  if (is_open_ == true) {
    r = ISCSDKLib::GetImage(pallarax_, image_, 0);
    if (r == 0) {
      r = ISCSDKLib::GetDepthInfo(depth_);
      /*
       * Convert the original position to Upper-Left
       */
      switch (model_) {
        case 0:                         // VM
          {
            int size = info_.nImageWidth * info_.nImageHeight;
            uint8_t * sp0 = image_ + (size - 1);
            uint8_t * dp0 = left_image;
            uint8_t * sp1 = pallarax_ + (size - 1);
            uint8_t * dp1 = right_image;
            for (uint y = 0; y < info_.nImageHeight; y++) {
              for (uint x = 0; x < info_.nImageWidth; x++) {
                *dp0++ = *sp0--;
                *dp1++ = *sp1--;
              }
            }
            ret = true;
          }
          break;
        case 1:                         // XC
          {
            int w = info_.nImageWidth;
            int size = w * info_.nImageHeight;
            uint8_t * sp0 = image_ + (size - w);
            uint8_t * dp0 = left_image;
            uint8_t * sp1 = pallarax_ + (size - w);
            uint8_t * dp1 = right_image;
            for (uint y = 0; y < info_.nImageHeight; y++) {
              memcpy(dp0, sp0, info_.nImageWidth);
              sp0 -= w;
              dp0 += info_.nImageWidth;
              memcpy(dp1, sp1, info_.nImageWidth);
              sp1 -= w;
              dp1 += info_.nImageWidth;
            }
            ret = true;
          }
          break;
        default:
          break;
      }
      /*
       * Set camera parameters into the image
       */
      {
        memcpy( left_image, &camera_param_, sizeof(camera_param_));
        memcpy( right_image, &camera_param_, sizeof(camera_param_));
      }
    }
  }
  lock_.unlock();

  return ret;
}

bool ISCWrapper::set_shutter_control( bool is_auto, int gain, int exposure)
{
  bool ret = false;
  if (is_open_ == true) {
    ret = true;
    int i = is_auto;
    if( i != shutter_auto_ ) {
      shutter_auto_ = i;
      lock_.lock();
      ISCSDKLib::SetShutterControlMode(shutter_auto_);
      shutter_gain_ = -1;
      shutter_exposure_ = -1;
      lock_.unlock();
    }
    if( shutter_auto_ == 0 ) {
      if( shutter_gain_ != gain ) {
        usleep(10*1000);
        lock_.lock();
        shutter_gain_ = gain;
        ISCSDKLib::SetGainValue(shutter_gain_);
        lock_.unlock();
      }
      if( shutter_exposure_ != exposure ) {
        usleep(10*1000);
        lock_.lock();
        shutter_exposure_ = exposure;
        ISCSDKLib::SetExposureValue(shutter_exposure_);
        lock_.unlock();
      }
    }
  }
  return ret;
}

bool ISCWrapper::set_noise_filter(int filter_level)
{
  bool ret = false;
  if (is_open_ == true) {
    ret = true;
    if( noise_filter_ != filter_level ) {
      lock_.lock();
      noise_filter_ = filter_level;
      ISCSDKLib::SetNoiseFilter(noise_filter_);
      lock_.unlock();
    }
  }
  return ret;
}

bool ISCWrapper::get_size(int * width, int * height)
{
  bool ret = false;
  if (is_open_ == true) {
    ret = true;
    *width = info_.nImageWidth;
    *height = info_.nImageHeight;
  }
  return ret;
}

bool ISCWrapper::get_camera_info(struct CameraParamInfo * info)
{
  bool ret = false;
  if (is_open_ == true) {
    ret = true;
    *info = info_;
  }
  return ret;
}

} // namespace isc_camera

